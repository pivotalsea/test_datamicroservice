/**
 * 
 */
package io.pivotal.data.aggregatorservice.service;

import io.pivotal.data.aggregatorservice.model.DriverIncorrectData;
import io.pivotal.data.aggregatorservice.model.DriverRevenueData;
import io.pivotal.data.aggregatorservice.model.RouteData;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;


/**
 * @author shuklk2
 *
 */
@Service
public class AggregatorService {

	/*
	@Primary
	@Bean
	RestTemplate restTemplate() {
	    return new RestTemplate();
	}
	*/
	
	@Autowired
	RestTemplate restTemplate;
	
	@HystrixCommand(fallbackMethod = "fallbackEventCalls")
	public long getTotalEvents() {
		return restTemplate.getForObject("https://EVENTSERVICE-IOT-V1/events/total", Long.class);
	}
	
	@HystrixCommand(fallbackMethod = "fallbackEventCalls")
	public long getMissedEvents() {
		return restTemplate.getForObject("https://EVENTSERVICE-IOT-V1/events/missed", Long.class);
	}
	
	@HystrixCommand(fallbackMethod = "fallbackEventCalls")
	public long getCorrectEvents() {
		return restTemplate.getForObject("https://EVENTSERVICE-IOT-V1/events/correct", Long.class);
	}
	
	@HystrixCommand(fallbackMethod = "fallbackEventCalls")
	public long processTime() {
		return restTemplate.getForObject("https://EVENTSERVICE-IOT-V1/events/proctime", Long.class);
	}
	
	public List<DriverRevenueData> getTop10EarningDrivers() {
		return restTemplate.getForObject("https://ANALYTICSERVICE-IOT-V1/analytics/top10earningdrivers", List.class);
	}
	
	public List<DriverIncorrectData> getTop10ErringDrivers() {
		return restTemplate.getForObject("https://ANALYTICSERVICE-IOT-V1/analytics/top10erringdrivers", List.class);
	}
	
	private long fallbackEventCalls() {
		return 100L;
	}
	
	private List<DriverRevenueData> fallbackAnalyticsEarningDriverCall() {
		return new ArrayList<DriverRevenueData>();
	}
	
	private List<DriverIncorrectData> fallbackAnalyticsErringDriverCall() {
		return new ArrayList<DriverIncorrectData>();
	}
	
	/*
	@HystrixCommand(fallbackMethod = "fallbackfreeTaxiCall")
	public List<List<String>> getFreeTaxiesList() {
		return restTemplate.getForObject("https://TAXISERVICE-IOT-V1/freetaxies/list", List.class);
	}
	
	@HystrixCommand(fallbackMethod = "fallbackRoutesCall")
	public List<RouteData> getTop10Routes() {
		return restTemplate.getForObject("https://TAXISERVICE-IOT-V1/routes/top10routes", List.class);
	}



	private List<RouteData> fallbackRoutesCall() {
		// return new ArrayList<RouteData>();
        RouteData rd1 = new RouteData ("c:79.80_to_c:79.80","460","2013-01-03 11:17:00","2013-01-03 15:34:00");
        RouteData rd2 = new RouteData ("c:78.81_to_c:79.80","409","2013-01-03 11:09:00","2013-01-03 15:34:00");
        RouteData rd3 = new RouteData ("c:79.80_to_c:78.81","385","2013-01-03 11:12:00","2013-01-03 15:34:00");
        RouteData rd4 = new RouteData ("c:79.80_to_c:80.79","357","2013-01-03 11:12:00","2013-01-03 15:34:00");
        RouteData rd5 = new RouteData ("c:80.78_to_c:79.80","335","2013-01-03 11:07:00","2013-01-03 15:34:00");
        RouteData rd6 = new RouteData ("c:78.81_to_c:78.80","307","2013-01-03 11:11:00","2013-01-03 15:34:00");
        RouteData rd7 = new RouteData ("c:79.80_to_c:78.80","304","2013-01-03 11:11:00","2013-01-03 15:34:00");
        RouteData rd8 = new RouteData ("c:78.80_to_c:78.81","295","2013-01-03 10:52:00","2013-01-03 15:34:00");
        RouteData rd9 = new RouteData ("c:80.79_to_c:79.80","293","2013-01-03 11:09:00","2013-01-03 15:34:00");
        RouteData rd10 = new RouteData ("c:78.80_to_c:78.80","284","2013-01-03 10:34:00","2013-01-03 15:34:00");
        ArrayList<RouteData> al = new ArrayList<RouteData>();
        al.add(rd1);
        al.add(rd2);
        al.add(rd3);
        al.add(rd4);
        al.add(rd5);
        al.add(rd6);
        al.add(rd7);
        al.add(rd8);
        al.add(rd9);
        al.add(rd10);
        return al;
	}
	
	private List<List<String>> fallbackfreeTaxiCall() {
		// return new ArrayList<List<String>>();
        
        List<String> ls1 = new StringData("CBE2015D17B8641EADA0BFAD797C86FF","40.705238","-74.007355").asList();
        List<String> ls2 = new StringData("225E832E746912559FCAB4692F0E6FBF","40.738914","-73.987045").asList();
        List<String> ls3 = new StringData("91296D6CF20114CDE4289E23A24A8021","40.648190","-73.786339").asList();
        List<String> ls4 = new StringData("0C5296F3C8B16E702F8F2E06F5106552","40.754139","-73.972168").asList();
        List<String> ls5 = new StringData("9F8B1F5E209FBFDCF49F5637B6FC3E59","40.723072","-73.998940").asList();
        List<String> ls6 = new StringData("1F9E2C27335E24C6A72069A4A550E3D5","40.740311","-73.986176").asList();
        List<String> ls7 = new StringData("2E01B2EE0027256AE954FFBEDEFC73EE","40.720863","-73.997643").asList();
        List<String> ls8 = new StringData("4732F08B4851C4638A1901D02E3DB12C","40.779438","-73.953453").asList();
        List<String> ls9 = new StringData("040EF850718DFA6812F26C29982508F5","40.721664","-73.984619").asList();
        List<String> ls10 = new StringData("FEBFBC9EAE0AC7495398AECE81A28029","40.763290","-73.969177").asList();
        List<String> ls11 = new StringData("963629F58EAB79A747869FC8672AF50E","40.740246","-73.986191").asList();
        List<String> ls12 = new StringData("A29ADFD648BEAD35ED7940010B501B50","40.705582","-74.007805").asList();
        List<String> ls13 = new StringData("072E0721AE3D3F91576E68BAC5F6A802","40.746475","-73.980453").asList();
        List<String> ls14 = new StringData("4C059BA8EA18A4B4BE05953621A78393","40.758404","-73.972214").asList();
        List<String> ls15 = new StringData("CF55BD128ECDE7BC97DCC234505FA5CF","40.771744","-73.958733").asList();
        
        List<List<String>> lls = new ArrayList<List<String>>();
        lls.add(ls1);
        lls.add(ls2);
        lls.add(ls3);
        lls.add(ls4);
        lls.add(ls5);
        lls.add(ls6);
        lls.add(ls7);
        lls.add(ls8);
        lls.add(ls9);
        lls.add(ls10);
        lls.add(ls11);
        lls.add(ls12);
        lls.add(ls13);
        lls.add(ls14);
        lls.add(ls15);
        return lls;
	}
	

    private class StringData {
        private String id;
        private String lat;
        private String longitude;

        StringData (String id, String lat, String longitude) {
            this.id = id;
            this.lat = lat;
            this.longitude = longitude;
        }

        List<String> asList () {
            List<String> list = new ArrayList<String>();
            list.add(id);
            list.add(lat);
            list.add(longitude);
            return list;
        }

    }
    */
}
