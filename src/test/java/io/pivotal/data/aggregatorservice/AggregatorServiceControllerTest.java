package io.pivotal.data.aggregatorservice;

import io.pivotal.data.aggregatorservice.controller.AggregatorServiceController;
import io.pivotal.data.aggregatorservice.service.AggregatorService;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by cq on 17/9/15.
 */
public class AggregatorServiceControllerTest {

    @InjectMocks
    AggregatorServiceController controller;

    @Mock
    AggregatorService service;

    private MockMvc mockMvc;

    @Before
    public void setUp() throws Exception {

        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();

    }

    @Test
    public void testTotalEvents() throws Exception {

        when(service.getTotalEvents()).thenReturn(300L);

        MvcResult response = mockMvc.perform(get("/totalevents")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        assertThat(response.getResponse().getContentAsString(), containsString("300"));
    }

    @Test
    public void testMissedEvents() throws Exception {

        when(service.getMissedEvents()).thenReturn(300L);

        MvcResult response = mockMvc.perform(get("/missedevents")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        assertThat(response.getResponse().getContentAsString(), containsString("300"));
    }

    @Test
    public void testProcessTime() throws Exception {

        when(service.processTime()).thenReturn(300L);

        MvcResult response = mockMvc.perform(get("/processtime")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();

        assertThat(response.getResponse().getContentAsString(), containsString("300"));
    }
}
